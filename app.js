const mongoose = require("mongoose");
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require("fs");
const multer  = require('multer');

app.use(cors());
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));


mongoose.connect("mongodb+srv://Meshcheriakov:Anna26062010@cluster0-fq82p.mongodb.net/FinalProject", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
});

const routes = require ('./routes');
routes (app);

const server = app.listen(3000, function() {
    console.log('leistening on port', server.address().port);
});
