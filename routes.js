const  get = require ('./routes/candidates/get');
const  post = require ('./routes/candidates/post');
const  put = require ('./routes/candidates/put');
const  del = require ('./routes/candidates/delete');
const  getCV = require ('./routes/candidates/cv/get_cv');
const  postCV = require ('./routes/candidates/cv/post_cv');
const  putCV = require ('./routes/candidates/cv/put_cv');
const  delCV = require ('./routes/candidates/cv/delete_cv');

module.exports = (app) => {
    get(app);
    post(app);
    put(app);
    del(app);
    getCV(app);
    postCV(app);
    putCV(app);
    delCV(app)
};
