const Candidate = require ('../../models/candidate');
const multer  = require('multer');
const fs = require("fs");

const upload = multer({ dest: './tmp'});

module.exports = (app) => {
    app.put("/candidates/:id", upload.single('avatar'), function (req, res) {
        const exention = req.file.originalname.split(".");
        const file = __dirname + '/image/' + req.params.id + "." + exention.pop();

        fs.rename(req.file.path, file, function(err) {
            if (err) {
                console.log(err);
                res.send(500);
            }
        });

                req.body.avatar = file;

                Candidate.updateOne({_id: req.params.id}, req.body, (err, data) => {
                    if (err) {
                        res.send({status: "Error", message: err});
                    }
                    else {
                        res.send({"Status": "Succsess", result: data });
                    }
                });
    });
};
