const Candidate = require ('../../models/candidate');


module.exports = (app) => {
    app.post("/candidates", function (req, res) {

        const candidate = new Candidate({
            name: req.body.name,
            lastName: req.body.lastName,
            city: req.body.city,
            country: req.body.country,
            email: req.body.email,
            phone: req.body.phone,
            repositoryLink: req.body.repositoryLink,
            facebookLink: req.body.facebookLink,
            linkedinLink: req.body.linkedinLink,
            resumeJobs: req.body.resumeJobs,
            levelEng: req.body.levelEng,
            isActive: req.body.isActive
        });

        candidate.save(function (err, data) {
            if (err) {
                res.send({status: "Error", message: err});
            }
            else {
                res.send({status: "Success", result: data});
            }
        });


    });
};
