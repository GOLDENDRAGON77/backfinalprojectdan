const Cv = require ('../../../models/cv/cv');

module.exports = (app) => {
    app.delete("/candidates/cvs/:id", function (req, res) {

        Cv.remove({_id: req.params.id}, (err) => {
            if (err) {
                res.send({status: "Error", message: err});
            }
            else {
                res.send({"Status": "Succsess"});
            }
        });
    });
};
