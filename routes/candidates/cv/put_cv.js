const Cv = require ('../../../models/cv/cv');

module.exports = (app) => {
    app.put("/candidates/cvs/:id", function (req, res) {

        Cv.updateOne({_id: req.params.id}, req.body, (err, data) => {
            if (err) {
                res.send({status: "Error", message: err});
            }
            else {
                res.send({"Status": "Succsess", result: data });
            }
        });

    });
};
