const Cv = require ('../../../models/cv/cv');

module.exports = (app) => {
    app.post("/candidates/cvs", function (req, res) {

        const cv = new Cv({
            userId: req.body.userId,
            activeCV: req.body.activeCV,
            cvSkill: req.body.cvSkill
        });

        cv.save(function (err, data) {
            if (err) {
                res.send({status: "Error", message: err});
            }
            else {
                res.send({status: "Success", result: data});
            }
        });
    });
};
