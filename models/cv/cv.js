const mongoose = require("mongoose");

const Cv = mongoose.model("Cv", {
    userId: {
        type: String,
        ref: 'Candidate'
    },
    activeCV: {
        type: Boolean,
        required: true
    },
     cvSkill: {
        type: Array,
         required: true
     }
});



module.exports = Cv;
